#ifndef GETPLAYERINPUT_H
#define GETPLAYERINPUT_H
#include<string>
#include<vector>

//this is get player input interface to the user

//this takes player input and returns vector of words
std::vector<std::string> getInputn();

//this takes sentence and split it into vector
std::vector<std::string> split(const std::string& s);
//puts > before user input
void expInput();

//function that takes input
std::string userInput();



#endif