#include"Equiped.h"
#include"Object.h"
#include"StringFunctionality.h"
#include"GetPlayerInputFunctions.h"
#include<iostream>
// implementation of equiped class
using std::list;	using std::string;
using std::cin;		using std::cout;
using std::endl;	
//class of objects that player has currently equiped

typedef list<Object> objectContainer;

Equiped::Equiped()
{

}

Equiped::Equiped(objectContainer& objs)
{
	objects = objs;
}

Equiped::~Equiped()
{

}

void Equiped::addToEquiped(Object& toAdd)//object we want to add to equiped
{
	objects.push_back(toAdd);
}

objectContainer Equiped::getObjects()
{
	return objects; //take the vector which represents the equiped and puts it where we want it
}

void Equiped::printEquiped()
{
	cout << "Equiped " << "slots: arm chest hand "<< endl;
	for (objectContainer::iterator it = objects.begin(); it != objects.end(); ++it)
	{
		//print each object in equiped
		
		cout << "Item: "  << it->getName() << endl;

	}

}

void Equiped::removeFromEquiped(Object toRemove)
{
	bool itemFound = false;
	for (objectContainer::iterator it = objects.begin(); it != objects.end();++it)
	{
		if (it->getName() == toRemove.getName())
		{
			objects.erase(it); 
			itemFound = true;
			break;
		}
	}
	if (itemFound == false)//not necessary just precaution
	{
		cout << "Object not found" << endl;
	}
}

void Equiped::removeFromEquiped(string& toRemove)
{
	bool itemFound = false;
	for (objectContainer::iterator it = objects.begin(); it != objects.end(); ++it)
	{
		if (toLowerCase(it->getName()) == toLowerCase(toRemove))
		{
			objects.erase(it);
			itemFound = true;
			break;
		}
	}
	if (itemFound == false)
	{
		cout << "Object not found" << endl;
	}
}


bool Equiped::checkIsEquiped(string& toCheck)
{

	bool itemFound = false;
	for (objectContainer::iterator it = objects.begin(); it != objects.end(); ++it)
	{
		if (toLowerCase(it->getName()) == toLowerCase(toCheck))//check is done without case sensitivity
		{
			itemFound = true;
			break;
		}

	}

	if (itemFound == false)//not necessary just precaution
	{
		//cout << "Object is not equiped" << endl;
	}

	return itemFound;
}


Object* Equiped::getAnyEquiped(string& toCheck)
{

	bool itemFound = false;
	for (objectContainer::iterator it = objects.begin(); it != objects.end(); ++it)
	{

		if (toLowerCase(it->getName()) == toLowerCase(toCheck))
		{
			return &(*it);
			itemFound = true;
			break;
		}

	}

	if (itemFound == false)//not necessary just precaution
	{
		cout << "getAnyEquiped false" << endl;
	}
}

bool Equiped::checkForSlot(string slot)
{

	bool itemFound = false;
	for (objectContainer::iterator it = objects.begin(); it != objects.end(); ++it)
	{

		if (toLowerCase(it->open()) == toLowerCase(slot))
		{
			itemFound = true; //if it is same slot return true
			break;
		}

	}

	if (itemFound == false)//not necessary just precaution
	{
		//cout << "checkForSlot false" << endl;
	}

	return itemFound;

}
