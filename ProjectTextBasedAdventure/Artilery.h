#ifndef ARTILERY_H
#define ARTILERY_H

void artileryStart();
void StartUp();
int Fire(int number_killed);
int Initialize();
int CheckShot();
int DistanceCalc(double in_angle);

#endif