#ifndef ADDEDITEMS_H
#define ADDEDITEMS_H

#include"Player.h"
#include"LoadedObjects.h"
//this is added items interface to the user
//all aditional new objects resulting from players actions


void addGoldenArmor(Player& currPlayer,LoadedObjects& allObjs);
void addGoldenShield(Player& currPlayer, LoadedObjects& allObjs);
void addPotion(Player& currPlayer, LoadedObjects& allObjs);







#endif