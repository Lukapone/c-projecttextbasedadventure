#include"GetPlayerInputFunctions.h"
#include"StringFunctionality.h"
#include<cctype> //defines isspace
#include<iostream>

using std::vector;		using std::string;
using std::cin;			using std::cout;
using std::endl;
//implementation of functions that will get player input

//this takes sentence and returns vector of words
vector<string> getInputn()
{
	expInput();
	string sentence = "";
	//get line of input from user and store it
	
	getline(cin, sentence);

	vector<string> words = split(sentence);

	return words;

}


//this takes sentence and split it into vector
vector<string> split(const string& s)
{
	vector<string> ret;
	typedef string::size_type stringSize;
	stringSize i = 0;

	//loop throught the characters in sentence and sort them into worlds acording breaks
	while (i != s.size())
	{
		//check if there is space if is jump over it and if we are not at the end of sentence
		while (i != s.size()&& isspace(s[i]))
		{
			++i;
		}
		//get the beggining of the word
		stringSize j = i;
		//if there are no spaces and we are not at the end increase j
		while (j != s.size()&&!isspace(s[j]))
		{
			++j;
		}

		//if we found some nonwhite spaces put them into vector as one string
		if (i != j)
		{

			ret.push_back(s.substr(i, j - i));
			i = j;
		}
	}//stops at end of the sentence
	//return vector of strings
	return ret;

}

//puts > before user input
void expInput()
{
	cout << ">";
}
//function that takes input
string userInput()
{
	string word = "";

	//this ads > before input
	expInput();
	cin >> word;

	return word;
}








