#include"LoadedObjects.h"
#include"StringFunctionality.h"
#include<iostream>
using std::string;		using std::vector;
using std::cout;		using std::endl;
using std::ofstream;	using std::ifstream;
typedef vector<Object> objContainer;

LoadedObjects::LoadedObjects()
{

}


LoadedObjects::LoadedObjects(std::vector<Object>& objs, std::vector<Object>& notImportantObjs)
{
	objects = objs;
	notImportantOnes = notImportantObjs;
}


LoadedObjects::~LoadedObjects()
{

}

void LoadedObjects::loadAllObjects(ifstream& inFile)
{

	while (!inFile.eof())//loop throught all the files objects
	{
		Object loadedObj;
		loadedObj.loadObject(inFile);
		if (loadedObj.getName() == "")
		{
			//cout << "Discarding empty obj" << endl;
		}
		else
		{
			objects.push_back(loadedObj);
		}	
	}
	
}

void LoadedObjects::loadAllObjectsParts(ifstream& inFileStatic, ifstream& inFileChanging)
{

	while (!inFileStatic.eof())//loop throught all the files objects
	{
		Object loadedObj;
		loadedObj.loadStaticObjectParts(inFileStatic);
		loadedObj.loadChangingObjectParts(inFileChanging);
		if (loadedObj.getName() == "")
		{
			//cout << "Discarding empty obj" << endl;
		}
		else
		{
			objects.push_back(loadedObj);
		}

	}
}

//sorths the loaded objects betveen players inventory equiped and map rooms
void LoadedObjects::sortAllObjects(Map& currMap, Player& currPlayer)
{
	bool found = false;
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		if (it->getWhereIsObj() == "inventory")
		{
			currPlayer.pickUp(*it);
			//cout << "picking sorting" << endl;
			found = true;
		}
		else if (it->getWhereIsObj() == "equiped")
		{
			currPlayer.equip(*it);
			//cout << "equiping sorting" << endl;
			found = true;
		}
		else if (it->getWhereIsObj() == "nowhere")
		{
			
			//cout << "equiping sorting" << endl;
			found = true;
		}
		else
		{
			currMap.putObjectInRoom(it->getWhereIsObj(), *it);
			//cout << "puting into room sorting" << endl;
			//cout << "loaded obj address of "<<it->getName() << &(*it) << endl;
			found = true;
		}
	}
	if (found == false)
	{
		cout << "didnt find where to put obj" << endl;
	}

}
//set the position of the objects between player and rooms according changes in game
void LoadedObjects::setWhereitIS(string toChange,string actualWhereItis)
{
Object toClone;
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		if (toLowerCase(it->getName()) == toLowerCase(toChange))
		{
			//if the object is not important clone it for quick save that it will be returned into game
			if (it->getIsImportant() == false)
			{
				toClone = (*it);
				notImportantOnes.push_back(toClone);//push the object into vector that we save with quick save to reset the state of game
			}
			else
			{
				//do nothing just change where object is after
			}
				it->setWhereItIS(actualWhereItis);
		}
	}
}
//clone potion and adds it into players inventory and loaded objects for saving later if player wont use all potions
void LoadedObjects::addPotionAfterBattleWithClone(Player&currPlayer)
{
	Object toClone;
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		if (toLowerCase(it->getName()) == "potionb")
		{
			toClone = (*it);
			toClone.setWhereItIS("inventory");
			objects.push_back(toClone);
			currPlayer.pickUp(toClone);
			break;
		}
	}

}


void LoadedObjects::removeByName(string toRemove)
{

	bool ereased = false;
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		if (toLowerCase(it->getName()) == toLowerCase(toRemove))
		{
			objects.erase(it);
			ereased = true;
			break;//iterator is invalidated instead of walidating it again break after you found it
		}
	}
	if (ereased == false)
	{
		cout << "didnt ereased it" << endl;
	}
}

Object& LoadedObjects::getByName(string toGet)
{

	bool found = false;
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		if (toLowerCase(it->getName()) == toLowerCase(toGet))
		{
			return *it;
			found = true;
			break;//iterator is invalidated instead of walidating it again break after you found it
		}
	}
	if (found == false)
	{
		cout << "didnt found it" << endl;
	}
}
//full game save that tracks all the objects
void LoadedObjects::saveAllObjects(ofstream& oFile)
{

	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		it->saveObject(oFile);
	}


}
//do the full save and reset the state of the not important objects
void LoadedObjects::quickSaveAllObjects(ofstream& oFile)
{
	
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		it->saveObject(oFile);
	}
	for (objContainer::iterator ite = notImportantOnes.begin(); ite != notImportantOnes.end(); ite++)
	{
		ite->saveObject(oFile);
	}

}


void LoadedObjects::printAllObjs()
{
	for (objContainer::iterator it = objects.begin(); it != objects.end(); it++)
	{
		it->printObject();
	}

}


