//Simple arena game program that gives player life points and calculate damage based on attack and defence types
//author Lukas Ponik
//version 1.0
//date 10/29/2013
// implementation of main arena app
#include<iostream>
#include<string>
#include<vector>
#include <windows.h> 


#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
//#include <time.h>       /* time  http://www.cplusplus.com/reference/cstdlib/rand/ */ does not work because the numbers are generated at the same time               

#include "Player.h"
#include "Enemy.h"
#include"ArenaOptions.h"
#include "Menu.h"
#include "SetConsoleWindow.h" 
#include "Logos.h"
#include "PrintFunctionality.h"
#include"GetPlayerInputFunctions.h"
#include"ArenaMainApp.h"
#include"AddedItems.h"
#include"Enums.h"
#include"MathFunctionality.h"


//include usings
using std::cin;				using std::cout;
using std::endl;			using std::string;
using std::vector;

typedef vector<Enemy> enemyContainer;

void arenaStartApp(Player& currPlayer,LoadedObjects& allObjs)
{
	signUpPlayer();
	enemyContainer enemyes = enemysToFight(); // names to give to enemies
	enemyes.shrink_to_fit();//if we have loads of enemys we can save some memory
	//counter that count killed enemies
	int eCounter = 0;
	bool victory = false;
	Enemy* enemy;
	do
	{
		//puts enemy pointer from vector into enemy to fight
		enemy = &enemyes[eCounter];
		do
		{
			addAttackBonuses((*enemy), currPlayer);
			//check if you are still alive and can fight after the swing
			if (currPlayer.isDeath()==true)
			{
				if (enemy->isDeath() == true)
				{
					cout << enemy->getName() << " killed you before he died " << endl;
					//add him to the killed enemies
					eCounter += 1;
					//exits first loop
					break;
				}
				else
				{
					cout << enemy->getName() << " killed you " << endl;
					//exits first loop
					break;
				}
			}
			//this exits loop after you kill enemy
		} while (enemy->isDeath()==false); 

		//check if player is still alive
		if (currPlayer.isDeath() == false)
		{
			eCounter += 1;
			cout << "You killed " << enemy->getName() << endl;
			if (eCounter == 4)
			{
				printlnCenter("You killed enough of opponents you became the new lord of the arena !!!");
				printlnCenter("You got health potion to get some of that lost health back");
				addPotion(currPlayer,allObjs);
				printCup();
				victory = true;
				break;
				//exits program
			}
			if (surrenderOption()==true)
			{
				victory = true;//he wants to leave so he won against one oponent
				break;
			}
			println("Next enemy is comming to fight you");
		}
		//exits loop when player dies
	} while (currPlayer.isDeath() == false);

	if (victory == false)
	{
		//when you die prints picture of skull
		cout << "You killed " << eCounter << " enemies in total" << endl;
		println("You died GAME OVER");
		println("Maybe next time you do better");
		//print skull logo
		printSkull();
		system("pause");
		exit(0);
	}

	
}

void signUpPlayer()
{

	checkMenuInput();
	println("Please enter your name to sign up for arena");
	string name = "";
	name = userInput();
	println("Hello " + name);
	println("Now is your time to shine and show your skills ");

	println("You walk into arena and see first enemy approaching");
	printlnCenter("Prepare for battle !!!");
	//shield logo
	printShield();

}

//this creates and vector of enemys with names and pass it to the application
enemyContainer enemysToFight()//http://stackoverflow.com/questions/9963453/c-pointer-to-vector  i wanted return pointer but is uneficient
{
	enemyContainer enemyes;  //dont define size of the vector or it wont work!!!!!!!!!!!!!
	enemyes.push_back(Enemy("Ringo"));
	enemyes.push_back(Enemy("Cassius"));
	enemyes.push_back(Enemy("Sebastian"));
	enemyes.push_back(Enemy("Bane the Arena Lord"));

	return enemyes;
}

//add bonuses depending on players choice of attack
void addAttackBonuses(Enemy& enemy, Player& currPlayer)
{	
	int atkChosen =  attackChoice();

	//set player and enemy attack and block acording the choice
	if (atkChosen == 1)//reduced block
	{
		enemy.setBlockChance(-BONUSBLOCK);//-10%block
	}
	else//bonus atck
	{
		currPlayer.setAttack(+BONUSATACK);//+5atack
	}
	//simple fight function
	currPlayer.fight(enemy);
	//after swing set the waluest to default otherwise they will grow after each enemy
	if (atkChosen == 1)
	{
		enemy.setBlockChance(BONUSBLOCK);
	}
	else
	{
		currPlayer.setAttack(-BONUSATACK);
	}
}

void fightEnemy(Player& currPlayer, LoadedEnemies& allEnem,LoadedObjects& allObjs,Enemy& enemy)
{

	do
	{
		addAttackBonuses(enemy, currPlayer);
		//check if you are still alive and can fight after the swing
		if (currPlayer.isDeath() == true)
		{
			if (enemy.isDeath() == true)
			{
				cout << enemy.getName() << " killed you before he died " << endl;
				system("pause");
				//exits game
				exit(0);
			}
			else
			{
				cout << enemy.getName() << " killed you " << endl;
				system("pause");
				//exits game
				exit(0);
			}
		}
		//this exits loop after you kill enemy
	} while (enemy.isDeath() == false);

	cout << "You killed " << enemy.getName() << endl;
	currPlayer.addExperience(enemy.getExperience());
	if (currPlayer.getCurrLevel() == 6)
	{
	}
	else
	{
		cout << "You got " << enemy.getExperience() << " experience." << endl;
		currPlayer.checkForLvlUp();
	}
	printlnCenter("You got health potion to get some of that lost health back");
	addPotion(currPlayer, allObjs);

}


Enemy& chooseRandomEnemy(LoadedEnemies& allEnem)
{
	int choosenEnem = Random(0, allEnem.getAllEnemies()->size()-1);
	return allEnem.getAnyEnemy(choosenEnem);
}
//when the player walks from room to room checks if there is any enemy
bool checkForFight(Player& currPlayer, LoadedEnemies& allEnem,LoadedObjects& allObjs,string& whereYouGo)
{
	
	
	
	//puts enemy pointer from vector into enemy to fight
	Enemy& enemyRandom=chooseRandomEnemy(allEnem);
	//first we check if we have any enemy on the road that we skipped last time
	if (allEnem.checkForSave(whereYouGo) == true)
	{

		Enemy& savedOne = allEnem.getTheSavedOne(whereYouGo);//get the saved enemy fight it and set its position to nowhere
		fightEnemy(currPlayer, allEnem, allObjs, savedOne);
		savedOne.setPosition("none");
	}
	else
	{
		//50% chance to find enemy
		int roll = Random(0, 1);
		if (roll == 0)
		{
			//cout << "No enemy to fight on road" << endl;
			return false;
		}
		cout << "There was a bandit ont the road that attacked you " << endl;

		if (checkForRun() == true)//yes you run away
		{
			//save the enemy here that you run away from
			enemyRandom.setPosition(whereYouGo);
		}
		else
		{
			fightEnemy(currPlayer, allEnem, allObjs, enemyRandom);
		}
		
	}	
	return true;
}

bool checkForRun()
{
	cout << "Do you want to fight (f) or try to run(r)?" << endl;
	if (checkForOption("r", "f") == true)
	{
		int roll = Random(0, 1);
			//50%chance to run
		if(roll==1)
		{
			cout << "You were faster than bandit and escaped" << endl;
			return true;//yes you run away
		}
		else
		{
			cout << "You couldnt escape the battle" << endl;
			return false;
		}
	}
	return false;
}
