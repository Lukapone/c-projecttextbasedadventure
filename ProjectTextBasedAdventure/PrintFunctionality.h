#ifndef PRINTFUNCTIONALITY_H
#define PRINTFUNCTIONALITY_H
//this is print functionality interface to the user
#include<string>


//function that prints line of text into console
void println(const std::string line);
//centerred line
void printlnCenter(const std::string str);
//add string center option
std::string centerString(const std::string toCenter);
std::string centerString(const std::string toCenter, int minus);//returns center minus some spaces


#endif