#include"LoadedGames.h"
#include"MathFunctionality.h"
#include"Artilery.h"
#include"Hangman.h"
#include"TicTacToe.h"
#include<iostream>

using std::cout;	using std::endl;
using std::string;

LoadedGames::LoadedGames()
{

}


LoadedGames::LoadedGames(std::vector<std::string>& pGam)
{
	possibleGames = pGam;
}


LoadedGames::~LoadedGames()
{

}

//loads all the games and puts them into vector
void LoadedGames::loadAllGames(std::ifstream& inFile)
{
	string loadedGame;
	while (!inFile.eof())//loop throught all the files objects
	{
		inFile >> loadedGame;
		possibleGames.push_back(loadedGame);
	}
	possibleGames.shrink_to_fit();
}

//this function chooses random game from vector of possible games
void LoadedGames::loadRandomGame()
{
	int myChoosenGame = Random(0, possibleGames.size()-1);

	if (possibleGames[myChoosenGame] == "artilery")
	{
		cout << "Your random game was Artilery." << endl;
		artileryStart();
	}
	else if (possibleGames[myChoosenGame] == "hangman")
	{
		cout << "Your random game was Hangman." << endl;
		hangmanStart();
	}
	else
	{
		cout << "Your random game was TicTacToe." << endl;
		ticTacToeStart();
	}

}
