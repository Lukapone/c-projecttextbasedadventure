#include "PrintFunctionality.h"
#include"GetPlayerInputFunctions.h"
#include <string>

using std::string;
// implementation of simple menu
void showMenu()
{
	println("Are you sure you want to attend the matches ?");
	println("Yes (y) ");
	println("No (n) ");
}

void checkMenuInput()
{
	//show simple menu
	showMenu();
	//decide if to run or exit program
	string mChoice;
	//check the user choice
	do
	{
		mChoice = userInput();
		if (mChoice == "y" || mChoice == "Y")
		{
			break;
		}
		else if (mChoice == "n" || mChoice == "N")
		{
			system("pause");
			exit(0);
		}
		else
		{
			println("Not walid input");
		}
	} while (mChoice != "Y" || mChoice != "y" || mChoice != "n" || mChoice != "N");
}