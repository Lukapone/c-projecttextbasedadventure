//Simple adventure game program that gives player options of movement around the map and picking up the items
//author Lukas Ponik
//version 1.0
//date 11/25/2013
#include"Player.h"
#include"Map.h"
#include"LoadFunctions.h"
#include"GetPlayerInputFunctions.h"
#include"SetConsoleWindow.h"
#include"Parser.h"
#include"LoadedObjects.h"
#include"LoadedEnemies.h"
#include"LoadedGames.h"

#include<string>
#include<iostream>
#include<fstream>
#include<vector>


using std::string;		using std::vector;
using std::cout;		using std::cin;
using std::endl;		using std::ifstream;
using std::ostream;

//simple structure of game loop
int main()
{
	SetWindow(120, 50);
	




	Map myMap; //create map
	Player currPlayer; //create player
	LoadedObjects allObjects;//create storage for all of the objects loaded into game
	LoadedEnemies allEnemies;//storage for all enemies
	LoadedGames allPuzzles;//storage for all puzzles
	
	
	//functions that loads the files into map and object
	if (startFromSave() == true)
	{
		loadSavedPlayer(currPlayer);
		setupLoadedGame(myMap, currPlayer, allObjects);
		loadSavedEnemies(allEnemies);
		loadPossiblePuzzles(allPuzzles);
	}
	else
	{
		loadNewPlayer(currPlayer);
		setCurPlayersName(currPlayer);
		printStory(currPlayer);
		setupNewGame(myMap, currPlayer, allObjects);
		loadEnemies(allEnemies);
		loadPossiblePuzzles(allPuzzles);
	}
	

	//game loop
	bool done = false;
	while (!done)
	{
		vector<string> sentence;
		while (sentence.size() == 0)//this will get rid of 'no keywords enterred when he does not type anything
		{
			sentence = getInputn();//get input to parse
		}	
		//parse any input and give you answer
		parse(myMap, currPlayer, sentence, allObjects, allEnemies,allPuzzles);
	}

	return 0;
}