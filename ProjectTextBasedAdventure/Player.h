#ifndef PLAYER_H
#define PLAYER_H

//this is player class interface to the user
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//player class
#include<string>
#include"Inventory.h"
#include"Equiped.h"
#include<fstream>

class Enemy;

class Player
{
protected://set the variables to be private
	int health;
	int maxHealth;
	int armor;
	int attack;
	int blockChance;
	int level;
	int currExperience;
	std::string name;
	Inventory pInventory;//create new inventory for player
	Equiped pHasEquiped;//create new equiped 

public:
	//default constructor
	Player();
	Player(std::string nm);
	//full constructor
    Player(int arm, int atc, int blR, std::string nm,Inventory pInv,Equiped pHasEquiped,int level,int curExp,int maxH);
	//crate getters and setters
	double getHealth();
	void setHealth(int hel);
	void addHealth(int hel);

	void addMaxHealth(int hel);
	double getArmor();
	void setArmor(int arm);
	double getAttack();
	void setAttack(int atc);
	double getBlockChance();
	void setBlockChance(int blC);
	std::string getName();
	void setName(std::string nm);
	//simple print to see what is in variables
	void print();
	void fight(Enemy& e);
	bool isDeath();

	//add object to player inventory
	void pickUp(Object& toAdd);
	bool isInPInventory(std::string toCheck);
	bool isInEquiped(std::string toCheck);
	Inventory getInventory();
	void removeFromInventory(Object& toRemove);
	void removeFromInventory(std::string& toRemove);
	void equip(Object& toAdd);

	Equiped getEquiped();
	void removeFromEquiped(Object& toRemove);
	void removeFromEquiped(std::string& toRemove);
	bool checkUseWith(std::string& toCheck);

	void loadPlayer(std::ifstream& inFile);
	void savePlayer(std::ofstream& oFile);
	void addExperience(int amount);
	int getExperience();
	void checkForLvlUp();
	int getCurrLevel();
	//deconstructor
	~Player();
};


#endif


