#ifndef INVENTORY_H
#define INVENTORY_H
#include<string>
#include<list>
#include"Object.h"
//this is Inventory class interface to the user
class Inventory
{
private:
	//i would use forward_list hard to work with only erease_after
	std::list<Object> objects;//  http://www.cplusplus.com/reference/stl/  list constant time in inserting and deleting if we need to save memory vector more efficient
	//vector can be faster than list http://stackoverflow.com/questions/8742462/stdforward-list-and-stdforward-listpush-back with right implementation
	//you can add size...

public:
	Inventory();
	Inventory(std::list<Object>& objs);
	~Inventory();

	void addToInventory(Object& toAdd);//add object
	void removeFromInventory(Object toRemove);
	void removeFromInventory(std::string& toRemove);
	bool checkIsInInventory(std::string toCheck);

	Object* getObjectByName(std::string& toGet);

	std::list<Object> getObjects();//return all objects in inventory
	bool checkUseWith(std::string& toCheck);
	void printInventory();
	//getters and setters for name etc.
	

};




























#endif