// Description: A simple game with a little physics (Maggie Johnson)
//
//  main.cpp
//  artillary
//
//  adapted by Dermot Logue on 14/10/2013.
//  Copyright (c) 2013 Dermot Logue. All rights reserved.
//
/*
The first observation is the introductory text which is displayed once per program execution. We need a random number generator to define the enemy distance for each round.  We need a mechanism for getting the angle input from the player and this is placed in a loop since it repeats until we hit the enemy. We also need a function for calculating the distance and angle. Finally, we must keep track of how many shots it took to hit the enemy, as well as how many enemies we have hit during the program execution.
*/

#include <iostream>//https://moodle1314.dkit.ie/course/view.php?id=416
#include <cmath>
#include"Artilery.h"

using namespace std;   // not universally recommended, but here for convenience

const int NUM_SHELLS = 10;  // allowed 10 shells per target
const int MIN_DISTANCE = 200;  // min distance for a target
const int MAX_DISTANCE = 900;  // max distance for a target
const double INITIAL_VELOCITY = 200.0;  // initial velocity of 200 m/sec
const double GRAVITY = 32.2;  // gravity for distance calculation
const double PI = 3.1415;

// Returns the distance a shot travels given its angle.
int DistanceCalc(double in_angle){

	double time_in_air;
	// The following calculates how far the shot goes given
	// its angle of projection, velocity, and how long it stays
	// in the air.
	time_in_air = (2.0 * INITIAL_VELOCITY * sin(in_angle)) / GRAVITY;
	return (int)round((INITIAL_VELOCITY * cos(in_angle)) * time_in_air);
}

// Get user's angle input and calculates distance where shot lands.
// Returns the distance the shot lands.
int CheckShot() {

	int distance;
	double angle;
	cout << "Enter angle: " << endl;
	if (!(cin >> angle))
		return -1;

	// Convert to radians.
	angle = (angle * PI) / 180.0;
	distance = DistanceCalc(angle);
	return distance;
}

// Generate a random number for the enemy position.
int Initialize() {
	int enemy_position;

	// Initialize random seed.
	//srand((unsigned int)time(NULL));

	// Generate random number between MIN_DISTANCE and MAX_DISTANCE
	enemy_position = rand() % MAX_DISTANCE + MIN_DISTANCE;
	cout << "The enemy is " << enemy_position << " metres away!!!" << endl;
	return enemy_position;
}

// This function plays the game
int Fire(int number_killed) {

	int enemy, shots, hit;
	int distance;

	// Initialize variables.
	shots = NUM_SHELLS;
	enemy = Initialize();
	distance = 0;
	hit = 0;

	do {
		// Get the distance where shot lands & compare it to enemy position.
		distance = CheckShot();

		// Some error checking on the input.
		if (distance == -1) {
			cout << "Enter numbers only..." << endl;
			cin.clear();
			cin.ignore(10000, '\n');
			continue;
		}

		// Compare the enemy position with the computed distance.
		if (abs(enemy - distance) <= 1) {
			hit = 1;        // i.e. true, we have hit the enemy
			number_killed++;
			cout << "You hit him!!!" << endl;
			cout << "It took you " << NUM_SHELLS - shots + 1 << " shots." << endl;
			cout << "You have killed " << number_killed << " enemies." << endl;
		}
		else{
			shots--;
			if (distance > enemy) {
				cout << "You over shot by " << abs(enemy - distance) << endl;
			}
			else {
				cout << "You under shot by " << abs(enemy - distance) << endl;
			}
		}
	} while ((shots > 0) && (!(hit)));

	if (shots == 0)
		cout << "You have run out of ammo..." << endl;

	return number_killed;
}

// This shows the introductory screen.
void StartUp()
{
	cout << "---------- Welcome to the Artillery Game ----------" << endl;;
	cout << "An enemy spawns at a random distance from you. ";
	cout << "You input an angle (number) to set your mortar gun and the shot is fired. ";
	cout << "If your angle is correct, your mortar bomb will hit the enemy and kill him. ";
	cout << "If not, you get another chance to set the angle, and so on... ";
	cout << "----------- Eat your heart out GTAV :)  -----------" << endl;;
}

void artileryStart()
{
	StartUp(); // This displays the introductory script.
	char done;

	int killed = 0;

	do {
		killed = Fire(killed); // Fire() contains the main loop of each round.

		cout << "I see another one, care to shoot again? (Y/N) " << endl;
		cin >> done;
	} while (done != 'n');

	cout << "You killed " << killed << " of the enemy. Goodbye." << endl;
}