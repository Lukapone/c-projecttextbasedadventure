#ifndef EQUIPED_H
#define EQUIPED_H
#include<string>
#include<list>
#include"Object.h"
//this is Equiped class interface to the user
class Equiped
{
private:
	std::list<Object> objects;//you can add size...

public:
	//constructors
	Equiped();
	Equiped(std::list<Object>& objs);
	//deconstructor
	~Equiped();
	//getters setters and functions
	void addToEquiped(Object& toAdd);//add object
	void removeFromEquiped(Object toAdd);
	void removeFromEquiped(std::string& toRemove);
	bool checkIsEquiped(std::string& toCheck);
	std::list<Object> getObjects();//return all objects in inventory

	Object* getAnyEquiped(std::string& toCheck);
	bool checkForSlot(std::string slot);

	void printEquiped();


};

#endif