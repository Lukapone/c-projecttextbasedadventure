#ifndef LOADEDGAMES_H
#define LOADEDGAMES_H
#include<vector>
#include<string>
#include<fstream>

class LoadedGames
{
private:
	std::vector<std::string> possibleGames;


public:
	LoadedGames();
	LoadedGames(std::vector<std::string>& objs);
	~LoadedGames();
	void loadAllGames(std::ifstream& inFile);
	void loadRandomGame();

};










#endif