#include "LoadFunctions.h"
#include"GetPlayerInputFunctions.h"
#include"Player.h"
#include"PrintFunctionality.h"
#include<fstream>
#include<iostream>
#include<string>
//this is implementation of my load functions

using std::ifstream;		using std::cout;
using std::endl;			using std::string;
using std::ofstream;		using std::cin;
//function that loads the files into map and object
void setupNewGame(Map& myMap,Player& currPlayer,LoadedObjects& allObj)
{
	//cout << "A of map" << &myMap << endl;
	//get the file into program
	ifstream inFile("rooms.txt");
	if (inFile)
	{
		myMap.loadRooms(inFile);//load all room into map
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();
//get the file into program
	ifstream inFile2("objects.txt");
	//ifstream inFile2("savedObjects.txt");
	if (inFile2)
	{
		allObj.loadAllObjects(inFile2);//load all objects into  map
		allObj.sortAllObjects(myMap,currPlayer);//split objects between map and player
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile2.close();
	//print the first room
	ifstream posFile("roomPosition.txt");
	if (posFile)
	{
		myMap.loadPosition(posFile);//load players position
	}
	else
	{
		cout << "loading failed" << endl;
	}
	posFile.close();
	myMap.printCurrentRoom();
}

void setupLoadedGame(Map& myMap, Player& currPlayer, LoadedObjects& allObj)
{
	//cout << "A of map" << &myMap << endl;
	//get the file into program
	ifstream inFile("rooms.txt");
	if (inFile)
	{
		myMap.loadRooms(inFile);//load all room into map
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();
	//get the file into program

	ifstream inFile2("savedObjects.txt");
	if (inFile2)
	{
		
		allObj.loadAllObjects(inFile2);//load all objects into  map
		allObj.sortAllObjects(myMap, currPlayer);//split objects between map and player
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile2.close();
	//print the first room
	ifstream posFile("savedRoomPosition.txt");
	if (posFile)
	{
		myMap.loadPosition(posFile);//load players position
	}
	else
	{
		cout << "loading failed" << endl;
	}
	posFile.close();
	myMap.printCurrentRoom();
}

void setupGameParts(Map& myMap, Player& currPlayer, LoadedObjects& allObj)
{
	//cout << "A of map" << &myMap << endl;
	//get the file into program
	ifstream inFile("rooms.txt");
	if (inFile)
	{
		myMap.loadRooms(inFile);//load all room into map
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();
	//get the file into program
	ifstream inFileStatic("objectsStaticParts.txt");
	ifstream inFileChanging("objectsChangingParts.txt");
	if ((inFileStatic) && (inFileChanging))
	{
		allObj.loadAllObjectsParts(inFileStatic, inFileChanging);
		allObj.sortAllObjects(myMap, currPlayer);//split objects between map and player
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFileStatic.close();
	inFileChanging.close();
	//print the first room
	ifstream posFile("roomPosition.txt");
	if (posFile)
	{
		myMap.loadPosition(posFile);//load players position
	}
	else
	{
		cout << "loading failed" << endl;
	}
	posFile.close();
	myMap.printCurrentRoom();
}

void loadNewPlayer(Player& currPlayer)
{
	//get the file into program
	ifstream inFile("player.txt");
	if (inFile)
	{
		currPlayer.loadPlayer(inFile);//load all players details
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();

}

void loadSavedPlayer(Player& currPlayer)
{
	//get the file into program
	ifstream inFile("savedPlayer.txt");
	if (inFile)
	{
		currPlayer.loadPlayer(inFile);//load all players details
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();

}

void loadEnemies(LoadedEnemies& allEnem)
{
	//get the file into program
	ifstream inFile("enemies.txt");
	if (inFile)
	{
		allEnem.loadEnemies(inFile);
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();

}

void loadSavedEnemies(LoadedEnemies& allEnem)
{
	//get the file into program
	ifstream inFile("savedEnemies.txt");
	if (inFile)
	{
		allEnem.loadEnemies(inFile);
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();

}

void loadPossiblePuzzles(LoadedGames& allPuzzles)
{
	//get the file into program
	ifstream inFile("possibleGames.txt");
	if (inFile)
	{
		allPuzzles.loadAllGames(inFile);
	}
	else
	{
		cout << "loading failed" << endl;
	}
	inFile.close();
}

void printStory(Player& currPlayer)
{
	cout << "Type 'help' to show available commands." << endl;
	cout << "-------------------------------------------------------------------------------------------" << endl;
	cout << "You " << currPlayer.getName() <<  " are the hero on the path of adventure around the country." << endl;
	cout<<"Recently you heard that there is arena in the town of Perpontium." << endl;
	cout << "Now you are heading there to prove yourself, that you are the best fighter in the province." << endl;
	cout << "-------------------------------------------------------------------------------------------" << endl;
}

void setCurPlayersName(Player& currPlayer)
{
	cout << "--------------------------------------------------" << endl;
	cout << "Hello adventurer please enter you characters name." << endl;
	string name = "";
	name = userInput();
	currPlayer.setName(name);

}


void saveGame(LoadedObjects& allObj, Player& currPlayer, Map& currMap, LoadedEnemies& allEnem)
{
	
	ofstream oFile("savedObjects.txt");
	if (oFile)
	{
		allObj.saveAllObjects(oFile);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	oFile.close();

	ofstream oFile2("savedPlayer.txt");
	if (oFile)
	{
		currPlayer.savePlayer(oFile2);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	oFile2.close();

	ofstream oPosFile("savedRoomPosition.txt");
	if (oPosFile)
	{
		currMap.savePosition(oPosFile);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	oPosFile.close();

	ofstream enemFile("savedEnemies.txt");
	if (enemFile)
	{
		allEnem.saveEnemies(enemFile);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	enemFile.close();
}

void quickSaveGame(LoadedObjects& allObj, Player& currPlayer, Map& currMap,LoadedEnemies& allEnem)
{

	ofstream oFile("savedObjects.txt");
	if (oFile)
	{
		allObj.quickSaveAllObjects(oFile);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	oFile.close();

	ofstream oFile2("savedPlayer.txt");
	if (oFile)
	{
		currPlayer.savePlayer(oFile2);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	oFile2.close();

	ofstream oPosFile("savedRoomPosition.txt");
	if (oPosFile)
	{
		currMap.savePosition(oPosFile);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	oPosFile.close();

	ofstream enemFile("savedEnemies.txt");
	if (enemFile)
	{
		allEnem.saveEnemies(enemFile);
	}
	else
	{
		cout << "saving failed" << endl;
	}
	enemFile.close();
}

bool startFromSave()
{
	printlnCenter("----------------------------------------------");
	printlnCenter("Welcome to Be the best Fighter adventure game.");
	printlnCenter("----------------------------------------------");
	//cout << "Welcome to Be the best Fighter adventure game." << endl;
	cout << "Do you want to start new game type 'n' or continue from last save type 's' ?" << endl;
	string choice = "";
	bool flag = false;
	//this will loop unless player enters 1 ro 2 as his choice
	do
	{
		//this ads > before input
		expInput();
		cin >> choice;
		if (choice == "n" || choice == "s")
		{
			flag = true;
		}
		else
		{
			println("Only (n) or (s) allowed as input");
			//need to clear the buffer here

			cin.clear();//clears the buffer
			cin.ignore(80, '\n'); //ignores characters until end of line
		}
	} while (flag == false);

	if (choice == "s")
	{
		return true;
	}

	return false;
}
